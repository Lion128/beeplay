import tkinter as tk
import os


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.master = master
        self.pack()
        self.create_widgets()

    def openfile(self):
        # os.startfile("README.md")
        os.startfile("beeplay.py")

    def create_widgets(self):
        self.a = tk.Button(self)
        self.a["text"] = "Start\n(click me)"
        self.a["command"] = self.openfile
        self.a.pack(side="top")
        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="bottom")


root = tk.Tk()
app = Application(master=root)
app.mainloop()
