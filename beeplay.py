import cocos
from cocos.path import Path, Bezier
from cocos.actions.interval_actions import MoveTo, Bezier
from math import hypot
from random import randint, uniform
from cocos.layer.base_layers import Layer
from cocos.sprite import Sprite
from pyglet.window import key
from cocos.scene import Scene
from cocos.audio.pygame.music import mixer

mixer.init(44100, -16, 2, 2048)
mixer.music.load("exemple.ogg".encode())
mixer.music.play(0)

def update(dt, player, l, bee):
    bee.move(player)
    l.key_react(player)
    return(player, l)

def choose_destination():
    x0, y0 = randint(40, 600), randint(40, 440)
    return(x0, y0)

def bezier_points(bee):
    x = uniform(20-bee.x, 600-bee.x)
    y = uniform(20-bee.y, 420-bee.y)
    return(x, y)

class Level1(Scene):
    def __init__(self):
        super().__init__()
        background = cocos.layer.Layer()
        self.add(background)
        dec = cocos.sprite.Sprite('background.jpg', position=(320, 240), scale=1/2)
        background.add(dec)
        bee = Bee()
        self.add(bee)
        barriers = Barriers()
        self.add(barriers)
        coffee_map = cocos.layer.Layer()
        self.add(coffee_map)
        coffee = Coffee(7)
        coffee_map.add(coffee)
        player = Player()
        self.add(player)
        l = PlayerInput1()
        self.add(l)
        t = TLayer()
        #self.add(t)
        self.schedule(update, player, l, bee)
        


class Bee(Sprite):
    def __init__(self):
        super().__init__('bee.png', position=(320, 240))
        self.speed = 50
        self.coffee = 0
        self.create2 = 'active'
        self.state = 'ready'
        self.way_list = ['Bezier', 'chase', 'line']
        self.way = self.way_list[randint(0,2)]
        self.x0, self.y0 = choose_destination()
        self.time = 0

    def choose_track(self):
        new_way = self.way_list[randint(0,2)]
        return(new_way)
    
    def reached(self):
        return(self.x == self.x0 and self.y == self.y0)
        
    def move(self, player):
        if self.state == 'ready':
            if self.way == 'line':
                self.x0, self.y0 = choose_destination()
                t = hypot(self.x - self.x0, self.y - self.y0)/self.speed
                action1 = MoveTo((self.x0, self.y0), t)
                self.do(action1)
            elif self.way == 'chase':
                max_time = 200
            elif self.way == 'Bezier':
                bx, by = bezier_points(self)
                a = (0, 0) #Текущее положение - начало координат
                b = (bx, by) #Конечный пункт
                cx, cy = bezier_points(self) #Промежуточный пункт
                c = (cx, cy)
                self.x0 = self.x+bx
                self.y0 = self.y+by
                t = (hypot(bx, by)+hypot(cx, cy)+hypot(bx-cx, by-cy))/(self.speed*2)
                bp = cocos.path.Bezier(a, b, c, c)
                action2 = Bezier(bp, t)
                self.do(action2)   
            self.state = 'move'         
                
        elif self.state == 'move' and self.way == 'chase':
            a = player.x-self.x
            b = player.y-self.y
            c = hypot(a, b)
            self.x += 2*a/c
            self.y += 2*b/c
            self.time += 1            
            if self.time == 200:
                self.state = 'ready'
                self.time = 0
                self.way = self.choose_track()
                                
        elif self.state == 'move' and self.reached():
            self.state = 'ready'
            self.way = self.choose_track()
    
    def make2(self, twos):
        for two in twos.get_children():
            if not two.contains(bee.x, bee.y):
                two = Two()
                twos.add(two)


class PlayerInput1(Layer):
    is_event_handler = True

    def __init__(self):
        super().__init__()
        self.keys_pressed = set()

    def on_key_press(self, key, modifiers):
        self.keys_pressed.add(key)

    def on_key_release(self, key, modifiers):
        self.keys_pressed.remove(key)

    def key_react(self, player):
        if key.UP in self.keys_pressed:
            player.y += 3
        if key.DOWN in self.keys_pressed:
            player.y -= 3
        if key.RIGHT in self.keys_pressed:
            player.x += 3
        if key.LEFT in self.keys_pressed:
            player.x -= 3


class Player(Sprite):
    def __init__(self):
        super().__init__('player.png', position=(600, 40))
        self.state = 'alive'
        self.life = 5


class Two(Sprite):
    def __init__(self, state):
        super().__init__('two.png', position=(320, 240))
        self.state = state


class Barriers(Layer):
    def __init__(self):
        super().__init__()


class Coffee(Sprite):
    def __init__(self, time):
        super().__init__('coffee.png', position=(100, 200))
        self.time = time


class TLayer(cocos.layer.Layer):
    is_event_handler = True

    def __init__(self):
        cocos.layer.Layer.__init__(self)
        self.time = 0.0
        self.schedule_interval(self.update, 1)
        self.set_lives = 3
        self.lives = randint(1, 10)

    def update(self, dt):
        # self.remove(self.k)
        # self.clear() Нужно найти способ чистить экран
        for i in range(self.set_lives):
            self.k = cocos.sprite.Sprite('lives.png', position=(25 + 30 * i, 455), scale=1 / 5)
            y = self.k
            self.add(self.k)

        lives_flag = True
        #if lives_flag:
            #self.set_lives -= 1
            #self.k.kill()

        #sleep(1)
        #for sprite in self.get_children():
            #sprite.kill()




cocos.director.director.init()
cocos.director.director.run(Level1())
